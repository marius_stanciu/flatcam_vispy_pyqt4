from FlatCAMTool import FlatCAMTool

from GUIElements import RadioSet, FloatEntry
from PyQt4 import QtGui, QtCore


class Film(FlatCAMTool):

    toolName = "Film PCB Tool"

    def __init__(self, app):
        FlatCAMTool.__init__(self, app)

        # Title
        title_label = QtGui.QLabel("<font size=4><b>%s</b></font>" % self.toolName)
        self.layout.addWidget(title_label)

        # Form Layout
        tf_form_layout = QtGui.QFormLayout()
        self.layout.addLayout(tf_form_layout)

        # Type of object for which to create the film
        self.tf_type_obj_combo = QtGui.QComboBox()
        self.tf_type_obj_combo.addItem("Gerber")
        self.tf_type_obj_combo.addItem("Excellon")
        self.tf_type_obj_combo.addItem("Geometry")

        # we get rid of item1 ("Excellon") as it is not suitable for creating film
        self.tf_type_obj_combo.view().setRowHidden(1, True)
        self.tf_type_obj_combo.setItemIcon(0, QtGui.QIcon("share/flatcam_icon16.png"))
        self.tf_type_obj_combo.setItemIcon(2, QtGui.QIcon("share/geometry16.png"))

        self.tf_type_obj_combo_label = QtGui.QLabel("Object Type:")
        self.tf_type_obj_combo_label.setToolTip(
            "Specify the type of object for which to create the film.\n"
            "It can be of type: Gerber or Geometry."
        )
        tf_form_layout.addRow(self.tf_type_obj_combo_label, self.tf_type_obj_combo)

        # List of objects for which we can create the film
        self.tf_object_combo = QtGui.QComboBox()
        self.tf_object_combo.setModel(self.app.collection)
        self.tf_object_combo.setRootModelIndex(self.app.collection.index(0, 0, QtCore.QModelIndex()))
        self.tf_object_combo.setCurrentIndex(1)
        self.tf_object_label = QtGui.QLabel("Object:")
        self.tf_object_label.setToolTip(
            "Object for which to create the film.                        "
        )
        tf_form_layout.addRow(self.tf_object_label, self.tf_object_combo)

        # Type of Box Object to be used as an envelope for film creation
        # Within this we can create negative
        self.tf_type_box_combo = QtGui.QComboBox()
        self.tf_type_box_combo.addItem("Gerber")
        self.tf_type_box_combo.addItem("Excellon")
        self.tf_type_box_combo.addItem("Geometry")

        # we get rid of item1 ("Excellon") as it is not suitable for box when creating film
        self.tf_type_box_combo.view().setRowHidden(1, True)
        self.tf_type_box_combo.setItemIcon(0, QtGui.QIcon("share/flatcam_icon16.png"))
        self.tf_type_box_combo.setItemIcon(2, QtGui.QIcon("share/geometry16.png"))

        self.tf_type_box_combo_label = QtGui.QLabel("Box Type:")
        self.tf_type_box_combo_label.setToolTip(
            "Specify the type of object to be used as an container for\n"
            "film creation. It can be: Gerber or Geometry type.        "
        )
        tf_form_layout.addRow(self.tf_type_box_combo_label, self.tf_type_box_combo)

        # Box
        self.tf_box_combo = QtGui.QComboBox()
        self.tf_box_combo.setModel(self.app.collection)
        self.tf_box_combo.setRootModelIndex(self.app.collection.index(0, 0, QtCore.QModelIndex()))
        self.tf_box_combo.setCurrentIndex(1)

        self.tf_box_combo_label = QtGui.QLabel("Box:")
        self.tf_box_combo_label.setToolTip(
            "The actual object that is used a container for the\n "
            "selected object for which we create the film.")
        tf_form_layout.addRow(self.tf_box_combo_label, self.tf_box_combo)

        # Film Type
        self.film_type = RadioSet([{'label': 'Pos', 'value': 'pos'},
                                     {'label': 'Neg', 'value': 'neg'}])
        self.film_type_label = QtGui.QLabel("Film Type:")
        self.film_type_label.setToolTip(
            "Generate a Positive black film or a Negative film."
        )
        tf_form_layout.addRow(self.film_type_label, self.film_type)

        # Boundary for negative film generation

        self.boundary_entry = FloatEntry()
        self.boundary_label = QtGui.QLabel("Border:")
        self.boundary_label.setToolTip(
            "Specify a border around the object.\n"
            "Only for negative film."
        )
        tf_form_layout.addRow(self.boundary_label, self.boundary_entry)

        # Buttons
        hlay = QtGui.QHBoxLayout()
        self.layout.addLayout(hlay)
        hlay.addStretch()

        self.film_object_button = QtGui.QPushButton("Save Film")
        self.film_object_button.setToolTip(
            "Create a Film for the selected object, within\n"
            "the specified box. Does not create a new\n "
            "object, but modifies it."
        )
        hlay.addWidget(self.film_object_button)

        self.layout.addStretch()

        ## Signals
        self.film_object_button.clicked.connect(self.on_film_creation)
        self.tf_type_obj_combo.currentIndexChanged.connect(self.on_type_obj_index_changed)
        self.tf_type_box_combo.currentIndexChanged.connect(self.on_type_box_index_changed)

        ## Initialize form
        self.film_type.set_value('neg')
        self.boundary_entry.set_value(0.0)

    def on_type_obj_index_changed(self, index):
        obj_type = self.tf_type_obj_combo.currentIndex()
        self.tf_object_combo.setRootModelIndex(self.app.collection.index(obj_type, 0, QtCore.QModelIndex()))
        self.tf_object_combo.setCurrentIndex(0)

    def on_type_box_index_changed(self, index):
        obj_type = self.tf_type_box_combo.currentIndex()
        self.tf_box_combo.setRootModelIndex(self.app.collection.index(obj_type, 0, QtCore.QModelIndex()))
        self.tf_box_combo.setCurrentIndex(0)

    def run(self):
        FlatCAMTool.run(self)
        self.app.ui.notebook.setTabText(2, "Film Tool")

    def on_film_creation(self):

        name = self.tf_object_combo.currentText()
        boxname = self.tf_box_combo.currentText()
        border = self.boundary_entry.get_value()

        self.app.inform.emit("Generating Film ...")

        if self.film_type.get_value() == "pos":
            try:
                filename = QtGui.QFileDialog.getSaveFileName(caption="Export SVG positive",
                                                             directory=self.app.get_last_save_folder(), filter="*.svg")
            except TypeError:
                filename = QtGui.QFileDialog.getSaveFileName(caption="Export SVG positive")

            filename = str(filename)

            if str(filename) == "":
                self.app.inform.emit("Export SVG positive cancelled.")
                return
            else:
                self.app.export_svg_black(name, boxname, filename)
        else:
            try:
                filename = QtGui.QFileDialog.getSaveFileName(caption="Export SVG negative",
                                                             directory=self.app.get_last_save_folder(), filter="*.svg")
            except TypeError:
                filename = QtGui.QFileDialog.getSaveFileName(caption="Export SVG negative")

            filename = str(filename)

            if str(filename) == "":
                self.app.inform.emit("Export SVG negative cancelled.")
                return
            else:
                self.app.export_svg_negative(name, boxname, filename, border)

    def reset_fields(self):
        self.tf_object_combo.setRootModelIndex(self.app.collection.index(0, 0, QtCore.QModelIndex()))
        self.tf_box_combo.setRootModelIndex(self.app.collection.index(0, 0, QtCore.QModelIndex()))

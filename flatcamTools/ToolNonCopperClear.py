from FlatCAMTool import FlatCAMTool
from copy import copy,deepcopy
# from GUIElements import IntEntry, RadioSet, FCEntry
# from FlatCAMObj import FlatCAMGeometry, FlatCAMExcellon, FlatCAMGerber
from ObjectCollection import *
import time

class NonCopperClear(FlatCAMTool, Gerber):

    toolName = "Non-Copper Clearing Tool"

    def __init__(self, app):
        self.app = app

        FlatCAMTool.__init__(self, app)
        Gerber.__init__(self, steps_per_circle=self.app.defaults["gerber_circle_steps"])

        self.tools_frame = QtGui.QFrame()
        self.tools_frame.setContentsMargins(0, 0, 0, 0)
        self.layout.addWidget(self.tools_frame)
        self.tools_box = QtGui.QVBoxLayout()
        self.tools_box.setContentsMargins(0, 0, 0, 0)
        self.tools_frame.setLayout(self.tools_box)

        ## Title
        title_label = QtGui.QLabel("<font size=4><b>%s</b></font>" % self.toolName)
        self.tools_box.addWidget(title_label)

        ## Form Layout
        form_layout = QtGui.QFormLayout()
        self.tools_box.addLayout(form_layout)

        ## Object
        self.object_combo = QtGui.QComboBox()
        self.object_combo.setModel(self.app.collection)
        self.object_combo.setRootModelIndex(self.app.collection.index(0, 0, QtCore.QModelIndex()))
        self.object_combo.setCurrentIndex(1)
        self.object_label = QtGui.QLabel("Gerber:")
        self.object_label.setToolTip(
            "Gerber object to be cleared of excess copper.                        "
        )
        e_lab_0 = QtGui.QLabel('')

        form_layout.addRow(self.object_label, self.object_combo)
        form_layout.addRow(e_lab_0)

        #### Tools ####
        self.tools_table_label = QtGui.QLabel('<b>Tools Table</b>')
        self.tools_table_label.setToolTip(
            "Tools pool from which the algorithm\n"
            "will pick the ones used for copper clearing."
        )
        self.tools_box.addWidget(self.tools_table_label)

        self.tools_table = FCTable()
        self.tools_box.addWidget(self.tools_table)

        self.tools_table.setColumnCount(2)
        self.tools_table.setHorizontalHeaderLabels(['#', 'Diameter'])
        self.tools_table.setSortingEnabled(False)
        self.tools_table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)

        self.empty_label = QtGui.QLabel('')
        self.tools_box.addWidget(self.empty_label)

        #### Add a new Tool ####
        self.addtool_label = QtGui.QLabel('<b>Add/Delete Tool</b>')
        self.addtool_label.setToolTip(
            "Add/Delete a tool to the tool list."
        )
        self.tools_box.addWidget(self.addtool_label)

        grid1 = QtGui.QGridLayout()
        self.tools_box.addLayout(grid1)

        addtool_entry_lbl = QtGui.QLabel('Tool Dia:')
        addtool_entry_lbl.setToolTip(
            "Diameter for the new tool"
        )
        grid1.addWidget(addtool_entry_lbl, 0, 0)

        hlay = QtGui.QHBoxLayout()
        self.addtool_entry = LengthEntry()
        hlay.addWidget(self.addtool_entry)

        self.addtool_btn = QtGui.QPushButton('Add Tool')
        self.addtool_btn.setToolTip(
            "Add a new tool to the tool list\n"
            "with the specified diameter."
        )
        self.addtool_btn.setFixedWidth(80)
        hlay.addWidget(self.addtool_btn)
        grid1.addLayout(hlay, 0, 1)

        grid2 = QtGui.QGridLayout()
        self.tools_box.addLayout(grid2)

        self.deltool_btn = QtGui.QPushButton('Delete Tool')
        self.deltool_btn.setToolTip(
            "Delete a tool in the tool list\n"
            "by selecting a row in the tool table."
        )
        grid2.addWidget(self.deltool_btn, 0, 1)


        grid3 = QtGui.QGridLayout()
        self.tools_box.addLayout(grid3)

        e_lab_1 = QtGui.QLabel('')
        grid3.addWidget(e_lab_1, 0, 0)

        nccoverlabel = QtGui.QLabel('Overlap:')
        nccoverlabel.setToolTip(
            "How much (fraction of tool width) to overlap each pass.\n"
            "Adjust the value starting with lower values\n"
            "and increasing it, even over the tool dia value,\n"
            "if areas that should be cleared are still not cleared.\n"
            "Lower values = faster processing, faster execution on PCB.\n"
            "Higher values = slow processing and slow execution on CNC\n"
            "due of too many paths. "
        )
        grid3.addWidget(nccoverlabel, 1, 0)
        self.ncc_overlap_entry = FloatEntry()
        grid3.addWidget(self.ncc_overlap_entry, 1, 1)

        nccmarginlabel = QtGui.QLabel('Margin:')
        nccmarginlabel.setToolTip(
            "Bounding box margin."
        )
        grid3.addWidget(nccmarginlabel, 2, 0)
        self.ncc_margin_entry = FloatEntry()
        grid3.addWidget(self.ncc_margin_entry, 2, 1)

        # Method
        methodlabel = QtGui.QLabel('Method:')
        methodlabel.setToolTip(
            "Algorithm for non-copper clearing:<BR>"
            "<B>Standard</B>: Fixed step inwards.<BR>"
            "<B>Seed-based</B>: Outwards from seed.<BR>"
            "<B>Line-based</B>: Parallel lines."
        )
        grid3.addWidget(methodlabel, 3, 0)
        self.ncc_method_radio = RadioSet([
            {"label": "Standard", "value": "standard"},
            {"label": "Seed-based", "value": "seed"},
            {"label": "Straight lines", "value": "lines"}
        ], orientation='vertical', stretch=False)
        grid3.addWidget(self.ncc_method_radio, 3, 1)

        # Connect lines
        pathconnectlabel = QtGui.QLabel("Connect:")
        pathconnectlabel.setToolTip(
            "Draw lines between resulting\n"
            "segments to minimize tool lifts."
        )
        grid3.addWidget(pathconnectlabel, 4, 0)
        self.ncc_connect_cb = FCCheckBox()
        grid3.addWidget(self.ncc_connect_cb, 4, 1)

        contourlabel = QtGui.QLabel("Contour:")
        contourlabel.setToolTip(
            "Cut around the perimeter of the polygon\n"
            "to trim rough edges."
        )
        grid3.addWidget(contourlabel, 5, 0)
        self.ncc_contour_cb = FCCheckBox()
        grid3.addWidget(self.ncc_contour_cb, 5, 1)

        restlabel = QtGui.QLabel("Rest M.:")
        restlabel.setToolTip(
            "If checked, use 'rest machining'.\n"
            "Basically it will clear copper outside PCB features,\n"
            "using the biggest tool and continue with the next tools,\n"
            "from bigger to smaller, to clear areas of copper that\n"
            "could not be cleared by previous tool, until there is\n"
            "no more copper to clear or there are no more tools.\n"
            "If not checked, use the standard algorithm."
        )
        grid3.addWidget(restlabel, 6, 0)
        self.ncc_rest_cb = FCCheckBox()
        grid3.addWidget(self.ncc_rest_cb, 6, 1)

        self.generate_ncc_button = QtGui.QPushButton('Generate Geometry')
        self.generate_ncc_button.setToolTip(
            "Create the Geometry Object\n"
            "for non-copper routing."
        )
        self.tools_box.addWidget(self.generate_ncc_button)

        self.units = ''
        self.tools = []

        try:
            self.tools = [float(eval(dia)) for dia in self.app.defaults["gerber_ncctools"].split(",")]
        except:
            log.error("At least one tool diameter needed. Verify in Edit -> Preferences -> Gerber Object -> NCC Tools.")
            return

        self.ncc_overlap_entry.set_value(self.app.defaults["gerber_nccoverlap"])
        self.ncc_margin_entry.set_value(self.app.defaults["gerber_nccmargin"])
        self.ncc_method_radio.set_value(self.app.defaults["gerber_nccmethod"])
        self.ncc_connect_cb.set_value(self.app.defaults["gerber_nccconnect"])
        self.ncc_contour_cb.set_value(self.app.defaults["gerber_ncccontour"])
        self.ncc_rest_cb.set_value(self.app.defaults["gerber_nccrest"])

        self.build_ui()

        self.addtool_btn.clicked.connect(self.on_tool_add)
        self.deltool_btn.clicked.connect(self.on_tool_delete)
        self.tools_table.itemChanged.connect(self.on_tool_edit)

        self.generate_ncc_button.clicked.connect(self.on_ncc)

        self.tools_box.addStretch()


    def install(self, icon=None, separator=None, **kwargs):
        FlatCAMTool.install(self, icon, separator, **kwargs)

    def run(self):
        FlatCAMTool.run(self)
        try:
            self.tools = [float(eval(dia)) for dia in self.app.defaults["gerber_ncctools"].split(",")]
        except:
            log.error("At least one tool diameter needed. Verify in Edit -> Preferences -> Gerber Object -> NCC Tools.")
            return
        self.tools_frame.show()
        self.build_ui()
        self.app.ui.notebook.setTabText(2, "NCC Tool")

    def build_ui(self):

        try:
            # if connected, disconnect the signal from the slot on item_changed as it creates issues
            self.tools_table.itemChanged.disconnect()
        except:
            pass

        # updated units
        self.units = self.app.general_options_form.general_group.units_radio.get_value().upper()

        if self.units == "IN":
            self.addtool_entry.set_value(0.039)
        else:
            self.addtool_entry.set_value(1)

        self.tools.sort(reverse=False)
        n = len(self.tools)
        self.tools_table.setRowCount(n)
        tool_id = 0

        for tool in self.tools:
            tool_id += 1
            id = QtGui.QTableWidgetItem('%d' % int(tool_id))
            id.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            row_no = tool_id - 1
            self.tools_table.setItem(row_no, 0, id)  # Tool name/id

            # Make sure that the drill diameter when in MM is with no more than 2 decimals
            # There are no drill bits in MM with more than 3 decimals diameter
            # For INCH the decimals should be no more than 3. There are no drills under 10mils
            if self.units == 'MM':
                dia = QtGui.QTableWidgetItem('%.2f' % tool)
            else:
                dia = QtGui.QTableWidgetItem('%.3f' % tool)

            dia.setFlags(QtCore.Qt.ItemIsEnabled)
            self.tools_table.setItem(row_no, 1, dia)  # Diameter

        # make the diameter column editable
        for row in range(tool_id):
            self.tools_table.item(row, 1).setFlags(
                QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

        # all the tools are selected by default
        self.tools_table.selectColumn(0)
        #
        self.tools_table.resizeColumnsToContents()
        self.tools_table.resizeRowsToContents()

        vertical_header = self.tools_table.verticalHeader()
        vertical_header.hide()
        self.tools_table.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        horizontal_header = self.tools_table.horizontalHeader()
        horizontal_header.setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        horizontal_header.setResizeMode(1, QtGui.QHeaderView.Stretch)

        self.tools_table.setSortingEnabled(True)
        # sort by tool diameter
        # self.tools_table.sortItems(1)

        self.tools_table.setMinimumHeight(self.tools_table.getHeight())
        self.tools_table.setMaximumHeight(self.tools_table.getHeight())

        # we reactivate the signals after the after the tool adding as we don't need to see the tool been populated
        self.tools_table.itemChanged.connect(self.on_tool_edit)


    def on_tool_add(self):
        self.tools_table.itemChanged.disconnect()
        self.tools[:] = []
        for row in range(self.tools_table.rowCount()):
            old_tool_dia = float(self.tools_table.item(row, 1).text())
            self.tools.append(old_tool_dia)

        tool_dia = self.addtool_entry.get_value()
        if tool_dia in self.tools:
            self.app.inform.emit("Cancelled. Tool already in the list.")
            self.tools_table.itemChanged.connect(self.on_tool_edit)
            return
        else:
            self.tools.append(tool_dia)
            self.app.inform.emit("[SUCCESS] Tool added to the list.")
        self.build_ui()

    def on_tool_edit(self):
        self.tools_table.itemChanged.disconnect()
        self.tools[:] = []
        for row in range(self.tools_table.rowCount()):
            tool_dia = float(self.tools_table.item(row, 1).text())
            self.tools.append(tool_dia)
        self.app.inform.emit("[SUCCESS] Tool from the list was edited.")
        self.tools_table.itemChanged.connect(self.on_tool_edit)

    def on_tool_delete(self):
        self.tools_table.itemChanged.disconnect()
        row_selected = self.tools_table.currentRow()

        try:
            if self.tools_table.selectedItems():
                for row_sel in self.tools_table.selectedItems():
                    row = row_sel.row()
                    if row < 0:
                        continue
                    print(row)
                    tool_dia_del = float(self.tools_table.item(row, 1).text())
                    self.tools.remove(tool_dia_del)
        except AttributeError:
            self.app.inform.emit("Failed. Select a tool to delete.")
            return
        except Exception as e:
            log.debug(str(e))
        self.app.inform.emit("[SUCCESS] Tool was deleted from the list.")
        self.build_ui()

    # clear copper with 'standard' algorithm
    def on_ncc(self):
        self.app.report_usage("gerber_on_ncc_button")

        over = self.ncc_overlap_entry.get_value()
        margin = self.ncc_margin_entry.get_value()
        connect = self.ncc_connect_cb.get_value()
        contour = self.ncc_contour_cb.get_value()
        method = self.ncc_method_radio.get_value()

        name = self.object_combo.currentText()
        # Get source object.
        try:
            obj = self.app.collection.get_by_name(str(name))
        except:
            self.app.inform.emit("[error_notcl]Could not retrieve object: %s" % name)
            return "Could not retrieve object: %s" % name

        if over is None or margin is None:
            log.error("Overlap and margin values needed")
            return

        # Sort tools in descending order
        self.tools.sort(reverse=True)

        # Prepare non-copper polygons
        try:
            bounding_box = obj.solid_geometry.envelope.buffer(distance=margin, join_style=JOIN_STYLE.mitre)
        except AttributeError:
            self.app.inform.emit("[error_notcl]No Gerber file available.")
            return

        empty = obj.get_empty_area(bounding_box)
        if type(empty) is Polygon:
            empty = MultiPolygon([empty])

        # Main procedure
        def clear_non_copper():
            cleared_geo = []
            # Already cleared area
            cleared = MultiPolygon()

            # Geometry object creating callback
            def geo_init(geo_obj, app_obj):
                geo_obj.options["cnctooldia"] = tool
                geo_obj.solid_geometry = []
                geo_obj.solid_geometry = deepcopy(cleared_geo)
                cleared_geo[:] = []

            # Generate area for each tool
            offset = sum(self.tools)
            for tool in self.tools:
                # Get remaining tools offset
                offset -= (tool - 1e-12)

                # Area to clear
                area = empty.buffer(-offset).difference(cleared)

                # Transform area to MultiPolygon
                if type(area) is Polygon:
                    area = MultiPolygon([area])

                if area.geoms:
                    # Check if area not empty
                    if len(area.geoms) > 0:
                        for p in area.geoms:
                            try:
                                if method == 'standard':
                                    cp = self.clear_polygon(p, tool, self.app.defaults["gerber_circle_steps"],
                                                            overlap=over, contour=contour, connect=connect)
                                elif method == 'seed':
                                    cp = self.clear_polygon2(p, tool, self.app.defaults["gerber_circle_steps"],
                                                             overlap=over, contour=contour, connect=connect)
                                elif method == 'lines':
                                    cp = self.clear_polygon3(p, tool, self.app.defaults["gerber_circle_steps"],
                                                             overlap=over, contour=contour, connect=connect)
                                cleared_geo.append(list(cp.get_objects()))
                            except:
                                log.warning("Polygon can not be cleared.")

                        # check if there is a geometry at all in the cleared geometry and only if there is any create
                        # a new object
                        if cleared_geo:
                            # Overall cleared area
                            cleared = empty.buffer(-offset * (1 + over)).buffer(-tool / 1.999999).buffer(
                                tool / 1.999999)
                            # Create geometry object
                            name = obj.options["name"] + "_ncc_" + repr(tool) + "D"
                            self.app.new_object("geometry", name, geo_init)
                    else:
                        return

        # clear copper with 'rest-machining' algorithm
        def clear_non_copper_rest():
            cleared_geo = []
            cleared = []
            # Already cleared area
            cleared_cleaned = MultiPolygon()

            # Geometry object creating callback
            def geo_init(geo_obj, app_obj):
                geo_obj.options["cnctooldia"] = tool
                geo_obj.solid_geometry = []
                geo_obj.solid_geometry = deepcopy(cleared_geo)
                cleared_geo[:] = []

            area = empty.buffer(0)
            # Generate area for each tool
            while self.tools:
                tool = self.tools.pop(0)
                tool_used = tool  - 1e-12
                cleared_geo[:] = []

                # Area to clear
                for poly in cleared:
                    area = area.difference(poly)

                # Transform area to MultiPolygon
                if type(area) is Polygon:
                    area = MultiPolygon([area])

                if area.geoms:
                    # Check if area not empty
                    if len(area.geoms) > 0:
                        for p in area.geoms:
                            try:
                                if method == 'seed':
                                    cp = self.clear_polygon2(p, tool_used, self.app.defaults["gerber_circle_steps"],
                                                            overlap=over, contour=contour, connect=connect)
                                elif method == 'lines':
                                    cp = self.clear_polygon3(p, tool_used,
                                                             self.app.defaults["gerber_circle_steps"],
                                                             overlap=over, contour=contour, connect=connect)
                                else:
                                    cp = self.clear_polygon(p, tool_used,
                                                             self.app.defaults["gerber_circle_steps"],
                                                             overlap=over, contour=contour, connect=connect)
                                cleared_geo.append(list(cp.get_objects()))
                            except:
                                log.warning("Polygon can not be cleared.")

                        # check if there is a geometry at all in the cleared geometry and only if there is any create
                        # a new object
                        if cleared_geo:
                            # Overall cleared area
                            cleared_area = list(self.flatten_list(cleared_geo))
                            cleared[:] = []

                            # cleared = MultiPolygon([p.buffer(tool_used / 2).buffer(-tool_used / 2)
                            #                         for p in cleared_area])

                            buffer_value = tool_used / 2
                            for p in cleared_area:
                                poly = p.buffer(buffer_value)
                                poly = poly.buffer(-buffer_value)
                                cleared.append(poly)

                            # Create geometry object
                            name = obj.options["name"] + "_ncc_" + "%.2f" % tool + "D"
                            self.app.new_object("geometry", name, geo_init)
                    else:
                        return
                else:
                    return

        # Do job in background
        proc = self.app.proc_container.new("Clearing non-copper areas.")

        def job_thread(app_obj):
            try:
                if self.ncc_rest_cb.get_value():
                    # clear copper with 'rest-machining' algorithm
                    clear_non_copper_rest()
                else:
                    clear_non_copper()
            except Exception as e:
                proc.done()
                raise e

            # Switch notebook to project page
            self.app.ui.notebook.setCurrentWidget(self.app.ui.project_tab)
            self.app.ui.notebook.setTabText(2, "Tools")
            self.tools_frame.hide()
            proc.done()

        self.app.worker_task.emit({'fcn': job_thread, 'params': [self.app]})
